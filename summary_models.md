
## 1 Initial model
```
Scorer functions used:
	{'scorer_1': make_scorer(balanced_accuracy_score), 'scorer_2': make_scorer(precision_score), 'scorer_3': make_scorer(recall_score)}
Scorer for refit: scorer_1
Tuning ranges are:
	{'scaler': [None, StandardScaler(copy=True, with_mean=True, with_std=True), MinMaxScaler(copy=True, feature_range=(0, 1))], 'classifier__C': array([1.e-05, 1.e-04, 1.e-03, 1.e-02, 1.e-01, 1.e+00, 1.e+01, 1.e+02,
       1.e+03, 1.e+04]), 'feature_selection__k': array([ 10,  30,  50,  70,  90, 110, 130, 150, 170, 190, 210, 230, 250,
       270, 290])}

with 5 splits
mean refit time: 0.250 seconds
mean score time: 0.003 seconds
best params in grid search cv are:
	{'classifier__C': 0.1, 'feature_selection__k': 190, 'scaler': None}
training score 0.965 (+/- 0.023)
cv score 0.680 (+/- 0.198)
test score 0.555
Grid Search CV took 267.091 seconds


Additional perfomance metrics (not considered in grid search):

For scorer scorer_2 ("make_scorer(precision_score)")
training score 0.257 (+/- 0.114)
cv score 0.115 (+/- 0.095)
test score 0.053


For scorer scorer_3 ("make_scorer(recall_score)")
training score 1.000 (+/- 0.000)
cv score 0.450 (+/- 0.429)
test score 0.200
```

## 2 Model with augmentation

```


Scorer functions used:
	{'scorer_1': make_scorer(balanced_accuracy_score), 'scorer_2': make_scorer(precision_score), 'scorer_3': make_scorer(recall_score)}
Scorer for refit: scorer_1
Tuning ranges are:
	{'scaler': [None, StandardScaler(copy=True, with_mean=True, with_std=True), MinMaxScaler(copy=True, feature_range=(0, 1))], 'classifier__C': array([1.e-01, 1.e+00, 1.e+01, 1.e+02, 1.e+03, 1.e+04, 1.e+05, 1.e+06]), 'feature_selection__k': array([ 10,  30,  50,  70,  90, 110, 130, 150, 170, 190, 210, 230, 250,
       270, 290])}

with 5 splits
mean refit time: 0.107 seconds
mean score time: 0.004 seconds
best params in grid search cv are:
	{'classifier__C': 100.0, 'feature_selection__k': 290, 'scaler': StandardScaler(copy=True, with_mean=True, with_std=True)}
training score 1.000 (+/- 0.000)
cv score 0.968 (+/- 0.027)
test score 0.889
Grid Search CV took 228.465 seconds


Additional perfomance metrics (not considered in grid search):

For scorer scorer_2 ("make_scorer(precision_score)")
training score 1.000 (+/- 0.000)
cv score 0.805 (+/- 0.143)
test score 0.771


For scorer scorer_3 ("make_scorer(recall_score)")
training score 1.000 (+/- 0.000)
cv score 0.976 (+/- 0.064)
test score 0.818

```

## 3 Model with augmentation in running_difference mode

```

Scorer functions used:
	{'scorer_1': make_scorer(balanced_accuracy_score), 'scorer_2': make_scorer(precision_score), 'scorer_3': make_scorer(recall_score)}
Scorer for refit: scorer_1
Tuning ranges are:
	{'scaler': [None, StandardScaler(copy=True, with_mean=True, with_std=True), MinMaxScaler(copy=True, feature_range=(0, 1))], 'classifier__C': array([1.e-01, 1.e+00, 1.e+01, 1.e+02, 1.e+03, 1.e+04, 1.e+05, 1.e+06]), 'feature_selection__k': array([ 10,  30,  50,  70,  90, 110, 130, 150, 170, 190, 210, 230, 250,
       270, 290])}

with 5 splits
mean refit time: 0.075 seconds
mean score time: 0.003 seconds
best params in grid search cv are:
	{'classifier__C': 1000.0, 'feature_selection__k': 210, 'scaler': MinMaxScaler(copy=True, feature_range=(0, 1))}
training score 1.000 (+/- 0.000)
cv score 0.975 (+/- 0.014)
test score 0.980
Grid Search CV took 302.711 seconds


Additional perfomance metrics (not considered in grid search):

For scorer scorer_2 ("make_scorer(precision_score)")
training score 1.000 (+/- 0.000)
cv score 0.791 (+/- 0.087)
test score 0.805


For scorer scorer_3 ("make_scorer(recall_score)")
training score 1.000 (+/- 0.000)
cv score 0.992 (+/- 0.032)
test score 1.000

```


## 4 Model with augmentation in running_difference mode without HFC/flux

```

Scorer functions used:
	{'scorer_1': make_scorer(balanced_accuracy_score), 'scorer_2': make_scorer(precision_score), 'scorer_3': make_scorer(recall_score)}
Scorer for refit: scorer_1
Tuning ranges are:
	{'scaler': [None, StandardScaler(copy=True, with_mean=True, with_std=True), MinMaxScaler(copy=True, feature_range=(0, 1))], 'classifier__C': array([1.e-01, 1.e+00, 1.e+01, 1.e+02, 1.e+03, 1.e+04, 1.e+05, 1.e+06]), 'feature_selection__k': array([ 10,  30,  50,  70,  90, 110, 130, 150, 170, 190, 210, 230, 250,
       270])}

with 5 splits
mean refit time: 0.094 seconds
mean score time: 0.004 seconds
best params in grid search cv are:
	{'classifier__C': 10000.0, 'feature_selection__k': 270, 'scaler': MinMaxScaler(copy=True, feature_range=(0, 1))}
training score 1.000 (+/- 0.000)
cv score 0.974 (+/- 0.014)
test score 0.968
Grid Search CV took 250.676 seconds


Additional perfomance metrics (not considered in grid search):

For scorer scorer_2 ("make_scorer(precision_score)")
training score 1.000 (+/- 0.000)
cv score 0.757 (+/- 0.108)
test score 0.717


For scorer scorer_3 ("make_scorer(recall_score)")
training score 1.000 (+/- 0.000)
cv score 1.000 (+/- 0.000)
test score 1.000
```

## 5 Model with augmentation in running_difference mode without HFC/flux with balanced falses

```


Scorer functions used:
	{'scorer_1': make_scorer(balanced_accuracy_score), 'scorer_2': make_scorer(precision_score), 'scorer_3': make_scorer(recall_score)}
Scorer for refit: scorer_1
Tuning ranges are:
	{'scaler': [None, StandardScaler(copy=True, with_mean=True, with_std=True), MinMaxScaler(copy=True, feature_range=(0, 1))], 'classifier__C': array([1.e-01, 1.e+00, 1.e+01, 1.e+02, 1.e+03, 1.e+04, 1.e+05, 1.e+06]), 'feature_selection__k': array([ 10,  30,  50,  70,  90, 110, 130, 150, 170, 190, 210, 230, 250,
       270])}

with 5 splits
mean refit time: 0.092 seconds
mean score time: 0.005 seconds
best params in grid search cv are:
	{'classifier__C': 100.0, 'feature_selection__k': 270, 'scaler': MinMaxScaler(copy=True, feature_range=(0, 1))}
training score 0.998 (+/- 0.003)
cv score 0.957 (+/- 0.022)
test score 0.975
Grid Search CV took 250.990 seconds


Additional perfomance metrics (not considered in grid search):

For scorer scorer_2 ("make_scorer(precision_score)")
training score 0.974 (+/- 0.032)
cv score 0.678 (+/- 0.111)
test score 0.786


For scorer scorer_3 ("make_scorer(recall_score)")
training score 1.000 (+/- 0.000)
cv score 1.000 (+/- 0.000)
test score 1.000

```

## 6 Model with augmentation in running_difference mode without HFC/flux with balanced falses and extended length segments

```
Scorer functions used:
	{'scorer_1': make_scorer(balanced_accuracy_score), 'scorer_2': make_scorer(precision_score), 'scorer_3': make_scorer(recall_score)}
Scorer for refit: scorer_1
Tuning ranges are:
	{'scaler': [None, StandardScaler(copy=True, with_mean=True, with_std=True), MinMaxScaler(copy=True, feature_range=(0, 1))], 'classifier__C': array([1.e-02, 1.e-01, 1.e+00, 1.e+01, 1.e+02, 1.e+03, 1.e+04, 1.e+05,
       1.e+06, 1.e+07, 1.e+08, 1.e+09]), 'feature_selection__k': array([ 10,  30,  50,  70,  90, 110, 130, 150, 170, 190, 210, 230, 250,
       270, 290, 310, 330, 350, 370, 390])}

with 5 splits
mean refit time: 0.135 seconds
mean score time: 0.014 seconds
best params in grid search cv are:
	{'classifier__C': 1000000.0, 'feature_selection__k': 350, 'scaler': MinMaxScaler(copy=True, feature_range=(0, 1))}
training score 1.000 (+/- 0.000)
cv score 0.973 (+/- 0.015)
test score 0.986
Grid Search CV took 1351.089 seconds


Additional perfomance metrics (not considered in grid search):

For scorer scorer_2 ("make_scorer(precision_score)")
training score 1.000 (+/- 0.000)
cv score 0.770 (+/- 0.096)
test score 0.868


For scorer scorer_3 ("make_scorer(recall_score)")
training score 1.000 (+/- 0.000)
cv score 1.000 (+/- 0.000)
test score 1.000
```


## 7 Model with augmentation in running_difference mode without HFC/flux with balanced falses and extended length segments with f-.75 score

```
Scorer functions used:
	{'scorer_1': make_scorer(balanced_accuracy_score), 'scorer_2': make_scorer(precision_score), 'scorer_3': make_scorer(recall_score), 'scorer_4': make_scorer(f1_score), 'scorer_5': make_scorer(fbeta_score, beta=0.75)}
Scorer for refit: scorer_5
Tuning ranges are:
	{'scaler': [None, StandardScaler(copy=True, with_mean=True, with_std=True), MinMaxScaler(copy=True, feature_range=(0, 1))], 'classifier__C': array([1.e-02, 1.e-01, 1.e+00, 1.e+01, 1.e+02, 1.e+03, 1.e+04, 1.e+05,
       1.e+06, 1.e+07, 1.e+08, 1.e+09]), 'feature_selection__k': array([ 10,  30,  50,  70,  90, 110, 130, 150, 170, 190, 210, 230, 250,
       270, 290, 310, 330, 350, 370, 390])}

with 5 splits
mean refit time: 0.106 seconds
mean score time: 0.007 seconds
best params in grid search cv are:
	{'classifier__C': 1000000.0, 'feature_selection__k': 370, 'scaler': MinMaxScaler(copy=True, feature_range=(0, 1))}
training score 1.000 (+/- 0.000)
cv score 0.863 (+/- 0.035)
test score 0.928
Grid Search CV took 1253.532 seconds


Additional perfomance metrics (not considered in grid search):

For scorer scorer_1 ("make_scorer(balanced_accuracy_score)")
training score 1.000 (+/- 0.000)
cv score 0.978 (+/- 0.007)
test score 0.989


For scorer scorer_2 ("make_scorer(precision_score)")
training score 1.000 (+/- 0.000)
cv score 0.801 (+/- 0.046)
test score 0.892


For scorer scorer_3 ("make_scorer(recall_score)")
training score 1.000 (+/- 0.000)
cv score 1.000 (+/- 0.000)
test score 1.000


For scorer scorer_4 ("make_scorer(f1_score)")
training score 1.000 (+/- 0.000)
cv score 0.889 (+/- 0.029)
test score 0.943
```

## - Model with augmentation in running_difference mode without HFC/flux with balanced falses and extended length segments with f-.5 score

```

Scorer functions used:
	{'scorer_1': make_scorer(balanced_accuracy_score), 'scorer_2': make_scorer(precision_score), 'scorer_3': make_scorer(recall_score), 'scorer_4': make_scorer(f1_score), 'scorer_5': make_scorer(fbeta_score, beta=0.5)}
Scorer for refit: scorer_5
Tuning ranges are:
	{'scaler': [None, StandardScaler(copy=True, with_mean=True, with_std=True), MinMaxScaler(copy=True, feature_range=(0, 1))], 'classifier__C': array([1.e-02, 1.e-01, 1.e+00, 1.e+01, 1.e+02, 1.e+03, 1.e+04, 1.e+05,
       1.e+06, 1.e+07, 1.e+08, 1.e+09, 1.e+10, 1.e+11]), 'feature_selection__k': array([ 10,  30,  50,  70,  90, 110, 130, 150, 170, 190, 210, 230, 250,
       270, 290, 310, 330, 350, 370, 390])}

with 5 splits
mean refit time: 0.087 seconds
mean score time: 0.006 seconds
best params in grid search cv are:
	{'classifier__C': 10.0, 'feature_selection__k': 350, 'scaler': MinMaxScaler(copy=True, feature_range=(0, 1))}
training score 0.970 (+/- 0.016)
cv score 0.811 (+/- 0.107)
test score 0.855
Grid Search CV took 1428.342 seconds


Additional perfomance metrics (not considered in grid search):

For scorer scorer_1 ("make_scorer(balanced_accuracy_score)")
training score 0.997 (+/- 0.002)
cv score 0.974 (+/- 0.018)
test score 0.981


For scorer scorer_2 ("make_scorer(precision_score)")
training score 0.962 (+/- 0.020)
cv score 0.775 (+/- 0.122)
test score 0.825


For scorer scorer_3 ("make_scorer(recall_score)")
training score 1.000 (+/- 0.000)
cv score 1.000 (+/- 0.000)
test score 1.000


For scorer scorer_4 ("make_scorer(f1_score)")
training score 0.981 (+/- 0.010)
cv score 0.872 (+/- 0.078)
test score 0.904

```

## 8 Model with augmentation in running_difference mode without HFC/flux with balanced falses and extended length segments with f-.75 score and full hypertuning

```
Scorer functions used:
	{'scorer_1': make_scorer(balanced_accuracy_score), 'scorer_2': make_scorer(precision_score), 'scorer_3': make_scorer(recall_score), 'scorer_4': make_scorer(f1_score), 'scorer_5': make_scorer(fbeta_score, beta=0.75)}
Scorer for refit: scorer_5
Tuning ranges are:
	[{'scaler': [None, StandardScaler(copy=True, with_mean=True, with_std=True), MinMaxScaler(copy=True, feature_range=(0, 1)), MaxAbsScaler(copy=True), RobustScaler(copy=True, quantile_range=(25.0, 75.0), with_centering=True,
       with_scaling=True), PowerTransformer(copy=True, method='yeo-johnson', standardize=True), QuantileTransformer(copy=True, ignore_implicit_zeros=False, n_quantiles=1000,
          output_distribution='normal', random_state=None,
          subsample=100000), QuantileTransformer(copy=True, ignore_implicit_zeros=False, n_quantiles=1000,
          output_distribution='uniform', random_state=None,
          subsample=100000), Normalizer(copy=True, norm='l2')], 'feature_selection': [SelectKBest(k=330, score_func=<function f_classif at 0x2b9f7eed1c80>)], 'feature_selection__k': array([ 10,  30,  50,  70,  90, 110, 130, 150, 170, 190, 210, 230, 250,
       270, 290, 310, 330, 350, 370, 390]), 'classifier__C': array([1.e-02, 1.e-01, 1.e+00, 1.e+01, 1.e+02, 1.e+03, 1.e+04, 1.e+05,
       1.e+06, 1.e+07, 1.e+08, 1.e+09, 1.e+10, 1.e+11])}, {'scaler': [MinMaxScaler(copy=True, feature_range=(0, 1)), QuantileTransformer(copy=True, ignore_implicit_zeros=False, n_quantiles=1000,
          output_distribution='uniform', random_state=None,
          subsample=100000)], 'feature_selection': [SelectKBest(k=10, score_func=<function chi2 at 0x2b9f7eed1d90>)], 'feature_selection__k': array([ 10,  30,  50,  70,  90, 110, 130, 150, 170, 190, 210, 230, 250,
       270, 290, 310, 330, 350, 370, 390]), 'classifier__C': array([1.e-02, 1.e-01, 1.e+00, 1.e+01, 1.e+02, 1.e+03, 1.e+04, 1.e+05,
       1.e+06, 1.e+07, 1.e+08, 1.e+09, 1.e+10, 1.e+11])}, {}, {'scaler': [None, StandardScaler(copy=True, with_mean=True, with_std=True), MinMaxScaler(copy=True, feature_range=(0, 1)), MaxAbsScaler(copy=True), RobustScaler(copy=True, quantile_range=(25.0, 75.0), with_centering=True,
       with_scaling=True), PowerTransformer(copy=True, method='yeo-johnson', standardize=True), QuantileTransformer(copy=True, ignore_implicit_zeros=False, n_quantiles=1000,
          output_distribution='normal', random_state=None,
          subsample=100000), QuantileTransformer(copy=True, ignore_implicit_zeros=False, n_quantiles=1000,
          output_distribution='uniform', random_state=None,
          subsample=100000), Normalizer(copy=True, norm='l2')], 'feature_selection': [PCA(copy=True, iterated_power='auto', n_components=None, random_state=None,
  svd_solver='auto', tol=0.0, whiten=False)], 'feature_selection__n_components': array([ 10,  30,  50,  70,  90, 110, 130, 150, 170, 190, 210, 230, 250,
       270, 290, 310, 330, 350, 370, 390]), 'classifier__C': array([1.e-02, 1.e-01, 1.e+00, 1.e+01, 1.e+02, 1.e+03, 1.e+04, 1.e+05,
       1.e+06, 1.e+07, 1.e+08, 1.e+09, 1.e+10, 1.e+11])}]

with 5 splits
mean refit time: 0.614 seconds
mean score time: 1.176 seconds
best params in grid search cv are:
	{'classifier__C': 10000.0, 'feature_selection': SelectKBest(k=330, score_func=<function f_classif at 0x2b9f7eed1c80>), 'feature_selection__k': 330, 'scaler': QuantileTransformer(copy=True, ignore_implicit_zeros=False, n_quantiles=1000,
          output_distribution='uniform', random_state=None,
          subsample=100000)}
training score 1.000 (+/- 0.000)
cv score 0.928 (+/- 0.050)
test score 0.896
Grid Search CV took 1025.178 seconds


Additional perfomance metrics (not considered in grid search):

For scorer scorer_1 ("make_scorer(balanced_accuracy_score)")
training score 1.000 (+/- 0.000)
cv score 0.986 (+/- 0.017)
test score 0.983


For scorer scorer_2 ("make_scorer(precision_score)")
training score 1.000 (+/- 0.000)
cv score 0.896 (+/- 0.071)
test score 0.846


For scorer scorer_3 ("make_scorer(recall_score)")
training score 1.000 (+/- 0.000)
cv score 0.992 (+/- 0.031)
test score 1.000


For scorer scorer_4 ("make_scorer(f1_score)")
training score 1.000 (+/- 0.000)
cv score 0.941 (+/- 0.042)
test score 0.917

```

## 9 Model with augmentation in running_difference mode without HFC/flux with balanced falses and extended length segments with f-.75 score and specific (best options avoiding Quantile) hypertuning

```
Scorer functions used: 
	{'scorer_1': make_scorer(balanced_accuracy_score), 'scorer_2': make_scorer(precision_score), 'scorer_3': make_scorer(recall_score), 'scorer_4': make_scorer(f1_score), 'scorer_5': make_scorer(fbeta_score, beta=0.75)}
Scorer for refit: scorer_5
Tuning ranges are: 
	[{'scaler': [MinMaxScaler(copy=True, feature_range=(0, 1))], 'feature_selection': [SelectKBest(k=390, score_func=<function f_classif at 0x117998e18>)], 'feature_selection__k': array([ 10,  30,  50,  70,  90, 110, 130, 150, 170, 190, 210, 230, 250,
       270, 290, 310, 330, 350, 370, 390]), 'classifier__C': array([1.e-02, 1.e-01, 1.e+00, 1.e+01, 1.e+02, 1.e+03, 1.e+04, 1.e+05,
       1.e+06, 1.e+07, 1.e+08, 1.e+09, 1.e+10, 1.e+11])}, {'scaler': [MinMaxScaler(copy=True, feature_range=(0, 1))], 'feature_selection': [SelectKBest(k=270, score_func=<function chi2 at 0x117998f28>)], 'feature_selection__k': array([ 10,  30,  50,  70,  90, 110, 130, 150, 170, 190, 210, 230, 250,
       270, 290, 310, 330, 350, 370, 390]), 'classifier__C': array([1.e-02, 1.e-01, 1.e+00, 1.e+01, 1.e+02, 1.e+03, 1.e+04, 1.e+05,
       1.e+06, 1.e+07, 1.e+08, 1.e+09, 1.e+10, 1.e+11])}, {}, {'scaler': [MinMaxScaler(copy=True, feature_range=(0, 1))], 'feature_selection': [PCA(copy=True, iterated_power='auto', n_components=390, random_state=None,
  svd_solver='auto', tol=0.0, whiten=False)], 'feature_selection__n_components': array([ 10,  30,  50,  70,  90, 110, 130, 150, 170, 190, 210, 230, 250,
       270, 290, 310, 330, 350, 370, 390]), 'classifier__C': array([1.e-02, 1.e-01, 1.e+00, 1.e+01, 1.e+02, 1.e+03, 1.e+04, 1.e+05,
       1.e+06, 1.e+07, 1.e+08, 1.e+09, 1.e+10, 1.e+11])}]

with 5 splits
mean refit time: 0.122 seconds
mean score time: 0.008 seconds
best params in grid search cv are: 
	{'classifier__C': 10000000.0, 'feature_selection': SelectKBest(k=270, score_func=<function chi2 at 0x117998f28>), 'feature_selection__k': 270, 'scaler': MinMaxScaler(copy=True, feature_range=(0, 1))}
training score 1.000 (+/- 0.000)
cv score 0.874 (+/- 0.048)
test score 0.851 
Grid Search CV took 501.534 seconds


Additional perfomance metrics (not considered in grid search):

For scorer scorer_1 ("make_scorer(balanced_accuracy_score)")
training score 1.000 (+/- 0.000)
cv score 0.980 (+/- 0.009)
test score 0.975 


For scorer scorer_2 ("make_scorer(precision_score)")
training score 1.000 (+/- 0.000)
cv score 0.817 (+/- 0.066)
test score 0.786 


For scorer scorer_3 ("make_scorer(recall_score)")
training score 1.000 (+/- 0.000)
cv score 1.000 (+/- 0.000)
test score 1.000 


For scorer scorer_4 ("make_scorer(f1_score)")
training score 1.000 (+/- 0.000)
cv score 0.899 (+/- 0.040)
test score 0.880 

```