#!/bin/bash
#PBS -N master_thesis-full-hypertuning
#PBS -l nodes=1:ppn=all                     ## single-node job, all cores
#PBS -l walltime=00:20:00                   ## 20 minutes

# load appropriate modules
ml Python/3.6.6-intel-2018b
ml scikit-learn/0.20.0-intel-2018b-Python-3.6.6

# copy input data from location where job was submitted from
cp -r $PBS_O_WORKDIR/job/* $TMPDIR

# go to temporary working directory (on local disk)
cd $TMPDIR

# run
python ML_model.py hpc > out.log 2> err.log

# copy back output data, ensure unique filename using $PBS_JOBID
mkdir $VSC_DATA/master_thesis_results

cp out.log $VSC_DATA/master_thesis_results/out_${PBS_JOBID}.log
cp err.log $VSC_DATA/master_thesis_results/err_${PBS_JOBID}.log

cp fase_2/model_pipe.pkl $VSC_DATA/master_thesis_results/model_pipe_${PBS_JOBID}.pkl
cp fase_2/grid_search_cv_results.pkl $VSC_DATA/master_thesis_results/grid_search_cv_results_${PBS_JOBID}.pkl