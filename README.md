# About
Auxiliary files and drafts used in my dissertation "Creating coherent cross-fades in an automatic DJ software tool using a machine learning approach".

See project repo [here](https://bitbucket.org/automaticdj/dj)

The repository files are mainly used for machine learning training.

# Requirements

Python version: `3.6.6`

Requirements: `pip install -r requirements.txt`

# Copyright
Copyright 2019 Miroslav Kovalenko <MiroslavKovalenko@icloud.com> Auxiliary files and drafts used in my dissertation "Creating coherent cross-fades in an automatic DJ software tool using a machine learning approach". See [project repo](https://bitbucket.org/automaticdj/dj).

# License

Released under AGPLv3 license. See the [COPYING.txt](COPYING.txt) file for details.